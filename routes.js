var applications = require('./src/controllers/applications');
var entries = require('./src/controllers/entries');
var schedules = require('./src/controllers/schedules');
var kue = require('kue');

module.exports = function(app){

	//CRUD Routes
	//Routes for applications
	app.get('/apps', applications.getAll);
	app.post('/apps', applications.createOne);
	app.get('/apps/:app_id', applications.getOne);
	app.patch('/apps/:app_id', applications.updateOne);
	app.delete('/apps/:app_id', applications.deleteOne);

	//Routes for entries
	app.get('/apps/:app_id/entries', entries.getAll);
	app.get('/apps/:app_id/entries/:entry_id', entries.getOne);
	app.post('/apps/:app_id/entries', entries.createOne);
	app.patch('/apps/:app_id/entries/:entry_id', entries.updateOne);
	app.delete('/apps/:app_id/entries/:entry_id', entries.deleteOne);

	//Routes for schedules
	app.get('/apps/:app_id/schedule', schedules.getAll);
	app.post('/apps/:app_id/schedule', schedules.createOne);
	app.patch('/apps/:app_id/schedule/:schedule_id', schedules.updateOne);
	app.delete('/apps/:app_id/schedule/:schedule_id', schedules.deleteOne);

	//Less standard routes
	app.get('/apps/:app_id/current', applications.getCurrentEntry);

	//Panel
	app.use('/admin/jobs', kue.app);
};