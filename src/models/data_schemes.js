var mongoose = require('mongoose');
var serializer = require('./plugins/serializer');


/*
	Fields look like that:
	{"field_name" : "field_type"} => {"url" : String}
*/

var dataSchemeSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	fields : {
		type: mongoose.Schema.Types.Mixed,
		required: true
	}
});

dataSchemeSchema.plugin(serializer, {
	"name" : true,
	"fields" : true
});

module.exports = mongoose.model('DataScheme', dataSchemeSchema);