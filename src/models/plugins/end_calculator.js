module.exports = function(schema){

	schema.pre('save', function(next){
		this.ends_at = this.starts_at + this.duration;
		next();
	});

	schema.pre('update', function(next){
		this.ends_at = this.starts_at + this.duration;
		next();
	});


};