var Serializer = require('../../lib/serializer');
var serializer = new Serializer();

module.exports = function(schema, serialization_schema){
	schema.methods.toJSON = function(){
		return serializer.serialize(this.toObject(), serialization_schema, this);
	};
};