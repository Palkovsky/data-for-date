var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var autoIncrement = require('mongoose-auto-increment');
var moment = require('moment-timezone');
var serializer = require('./plugins/serializer');
var end_calculator = require('./plugins/end_calculator');
var accessor = require('../lib/accessor');
var IntervalTree = require('../lib/trees/interval_tree');
var rescheduler = require('../jobs/reschedule');
autoIncrement.initialize(mongoose.connection);

var entryScheme = new mongoose.Schema({
	payload : {
		type: mongoose.Schema.Types.Mixed,
		default : {},
		required: true
	}
}, {
	minimize: false
});

entryScheme.plugin(timestamps);
entryScheme.plugin(serializer, {
	"_id" : {"as" : "id"},
	"payload" : true
});


var sheduleScheme = new mongoose.Schema({
	starts_at : {
		type: Number,
		required: true
	},
	duration : {
		type: Number,
		required: true
	},
	ends_at : {
		type: Number,
		required: false
	},
	recurring: {
		type: Boolean,
		required: true,
		default: false
	},
	entry_id: {
		type: String,
		ref: 'Entry',
		required: false
	}
},{
	minimize: false
});

sheduleScheme.plugin(timestamps);
sheduleScheme.plugin(end_calculator);
sheduleScheme.plugin(serializer, {
	"_id" : {"as" : "id"},
	"starts_at" : {
		"set" : function(entity, schedule){

			var offset = moment.tz.zone(schedule.parent().timezone).parse(Date.UTC(entity.starts_at)) * 60;

			return {
				"utc" : entity.starts_at,
				"converted" : entity.starts_at + offset,
				"formated" : {
					"utc" : moment.utc(entity.starts_at * 1000).format('DD-MM-YYYY HH:mm'),
					"converted" : moment.utc((entity.starts_at + offset) * 1000).format('DD-MM-YYYY HH:mm')
				}
			}
		}
	},
	"ends_at" : {
		"set" : function(entity, schedule){

			var offset = moment.tz.zone(schedule.parent().timezone).parse(Date.UTC(entity.ends_at)) * 60;

			return {
				"utc" : entity.ends_at,
				"converted" : entity.ends_at + offset,
				"formated" : {
					"utc" : moment.utc(entity.ends_at * 1000).format('DD-MM-YYYY HH:mm'),
					"converted" : moment.utc((entity.ends_at + offset) * 1000).format('DD-MM-YYYY HH:mm')
				}
			}			
		}
	},
	"duration" : true,
	"recurring" : true,
	"entry" : {
		"minimize" : false,
		"set" : function(entity, schedule){
			var entry = schedule.parent().entries.filter(function(e){
				return e.id == schedule.entry_id;
			}).pop();
			if(entry == null)
				return null;
			return {'id' : entry['id'], 'payload' : entry['payload']}
		}
	}
});


var applicationSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	data_scheme: {
		type: String,
		default: "custom",
		enum: ["custom", "youtube", "video"]
	},
	url: {
		type: String,
		required: true
	},
	timezone: {
		type: String,
		required: true,
		enum: require("../data/timezone")
	},
	entries: [entryScheme],
	schedules: [sheduleScheme],
	//this field is updated in cron task. it's used to provide best rt on /current endpoint
	current_schedule_id : {
		type: String,
		default: null
	}
});


applicationSchema.plugin(timestamps);
applicationSchema.plugin(serializer, {
	"_id" : {"as" : "id"},
	"name" : true,
	"data_scheme" : {"as" : "scheme"},
	"timezone" : true,
	"url" : true
});
applicationSchema.plugin(autoIncrement.plugin, 'Application');

applicationSchema.methods.getScheduleAsTree = function(offset){

	if(offset === undefined)
		offset = 0;

	var tree = new IntervalTree();
	var schedules = this.schedules;
	for(var i = 0; i < schedules.length; i++){
		var schedule = schedules[i];
		tree.insert([schedule.starts_at + offset, schedule.starts_at + schedule.duration + offset], schedule);
	}
	return tree;
};

applicationSchema.methods.getIntersections = function(low, high){
	var tree = this.getScheduleAsTree();
	return tree.findIntervals(tree.getRoot(), [low, high]);
};

applicationSchema.methods.isIntervalFree = function(low, high){
	var tree = this.getScheduleAsTree();
	return (tree.findIntervals(tree.getRoot(), [low, high]).length === 0);
};

applicationSchema.methods.reschedule = function(done){
	rescheduler.reschedule(this, done);
};

applicationSchema.methods.balance = function(done){
	
	var schedules = this.schedules;
	var last = this.schedules[schedules.length - 1];
	var toEnd = [];

	for(var i = 0; i < schedules.length; i++) {
	    var schedule = schedules[i];

	    if(schedule.recurring === true && schedule.ends_at < Date.now()/1000){
			toEnd.push(schedule);
			schedule.remove();
		}
	}

	for(var i = 0; i < toEnd.length; i++){
		toEnd[i].starts_at = last.ends_at;
		toEnd[i].ends_at = last.ends_at + toEnd[i].duration;
		this.schedules.push(toEnd[i]);
		last = toEnd[i];
	}

	this.save(function(err){
		done(err);
	});

};

module.exports = mongoose.model('Application', applicationSchema);