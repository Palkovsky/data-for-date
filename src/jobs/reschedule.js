var queue = require('../lib/queue');
var mongoose = require('mongoose');

//this job loops through applications and assigns worker to each, which calls
//.balance() method on it to find current schedule, handle recurring ones
function plan_rescheduling(done, delay){
	if(delay === undefined){
		delay = 0;
	}
 	queue.create('plan_rescheduling', {})
 		.delay(delay)
		.priority('normal')
		.attempts(1)
		.removeOnComplete(false)
		.save(function(err){
			if(err){
				console.error(err);
				done(err);
			}

			if(!err){
				done();
			}
		});
};


function reschedule(application, done){
	queue.create('rescheduling', application)
		.priority('high')
		.attempts(1)
		.removeOnComplete(true)
		.save(function(err){
			if(err){
				console.error(err);
				done(err);
			}

			if(!err){
				done();
			}			
		});
};


queue.process('rescheduling', 100, function(job, done){
	mongoose.model('Application').findOne({_id: job.data.id}).exec(function(err, application) {
		if(application && !err){
			application.balance(function(err){
				if(err){
					done(err);
				}else{
					done();
				}
			});
		}else{
			done(err);
		}
	});
});

queue.process('plan_rescheduling', function(job, done){

	console.log('plan_rescheduling...');

	mongoose.model('Application').find({}).exec(function(err, applications) {

		if(err){
			done(err);
			console.log('RESHEDLING NEXT');
			plan_rescheduling(function(err){}, 0);
		}
		
		if(!err){
			console.log('APSS: ' + applications.length);

			for(var i = 0; i < applications.length; i++){
				var application = applications[i];
				application.reschedule(function(err){});
			}

			done();

			plan_rescheduling(function(){}, 30000);
		}
		
	});
});

module.exports = {
	reschedule: function(application, done){
		reschedule(application, done);
	},
	rescheduling: function(done){
		plan_rescheduling(done);
	}
};