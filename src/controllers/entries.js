var ResponseBuilder = require('../lib/response');
var errors = require("../lib/errors/errors");
var sanitize = require("../lib/sanitizer");
var accessor = require("../lib/accessor");
var ajv = require("../lib/ajv/ajv");

var allowed_params = ["payload"];

module.exports = {
	getAll: function(req, res, next){
		accessor.fetchApplication(req, res, function(application){
			return new ResponseBuilder(res)
			   	.status(200)
			  	.meta(true, true)
		      	.payload(application.entries)
		      	.build()			
		});
	},

	getOne: function(req, res, next){
		accessor.fetchEntry(req, res, function(application, entry){
			return new ResponseBuilder(res)
				.status(200)
				.meta(true)
				.payload(entry)
				.build();
		});
	},
	updateOne: function(req, res, next){

		accessor.fetchEntry(req, res, function(application, entry, data_scheme){

			sanitize(req.body, allowed_params);

			var validation_schema = {
			  	"type" : "object",
			  	"properties" : {
			  		"payload" : {"type" : "object", "scheme" : data_scheme},
			  	}
			};

			var validator = ajv.compile(validation_schema);

			if(validator(req.body)){

				if('payload' in req.body){
					entry.payload = req.body.payload;
					entry.markModified('payload');
				}

				application.save(function(err){

					if(err){
						return new ResponseBuilder(res)
					     	.status(400)
					      	.meta(false)
					      	.errors(errors[400])
					      	.build()		      		
			      	}

			       return new ResponseBuilder(res)
					   .status(200)
					   .meta(true)
					   .payload(entry)
					   .build()	

				});

			}else{
				return new ResponseBuilder(res)
					.status(422)
					.meta(false)
					.errors(validator.errors)
					.build()				
			}

		});

	},

	deleteOne: function(req, res, next){

		accessor.fetchEntry(req, res, function(application, entry){

			entry.remove();

			application.save(function(err){
				if(err){
					return new ResponseBuilder(res)
			      		.status(400)
			      		.meta(false)
			      		.errors(errors[400])
			      		.build()				
				}

		     	return new ResponseBuilder(res)
			   		.status(204)
		     		.meta(true)
		      		.payload({})
		      		.build()							
			});

		});
	},
	
	createOne: function(req, res, next){

		accessor.fetchApplication(req, res, function(application, data_scheme){

			sanitize(req.body, allowed_params);

			var validation_schema = {
			  	"type" : "object",
			  	"properties" : {
			  		"payload" : {"type" : "object", "scheme" : data_scheme},
			  	},
			  	"required" : ["payload"]
			 };

			var validator = ajv.compile(validation_schema);

			if(validator(req.body)){

				var entry = application.entries.create(req.body);
				entry.markModified('payload');
				application.entries.push(entry);

				application.save(function(error){

					if(error){
						return new ResponseBuilder(res)
			      			.status(400)
			      			.meta(false)
			      			.errors(errors[400])
			      			.build()		
					}

					return new ResponseBuilder(res)
					   	.status(201)
					  	.meta(true)
				      	.payload(entry)
				      	.build()

				});

			}else{
				return new ResponseBuilder(res)
					.status(422)
					.meta(false)
					.errors(validator.errors)
					.build()				
			}

		});

	}
};
