var ResponseBuilder = require('../lib/response');
var Application = require("../models/applications");
var errors = require("../lib/errors/errors");
var sanitize = require("../lib/sanitizer");
var accessor = require("../lib/accessor");
var ajv = require("../lib/ajv/ajv");
var moment = require('moment-timezone');

var allowed_params = ["name", "data_scheme", "url", "timezone"];

module.exports = {
	getAll: function(req, res, next){
	    accessor.fetchApplications(req, res, function(applications) {
	      return new ResponseBuilder(res)
	      		.status(200)
	      		.meta(true, true)
	      		.payload(applications)
	      		.build()
	    });
	},

	getOne: function(req, res, next){

		accessor.fetchApplication(req, res, function(application, data_scheme){

			var entries = [];
			for(var i = 0; i < application.entries.length; i++)
				entries.push(application.entries[i].toJSON());
			var schedules = [];
			for(var i = 0; i < application.schedules.length; i++)
				schedules.push(application.schedules[i].toJSON());


			application = application.toJSON();
			application["entries"] = entries;
			application["schedules"] = schedules;
			application["field_scheme"] = data_scheme;

			return new ResponseBuilder(res)
			    .status(200)
			   	.meta(true)
			   	.payload(application)
		     	.build()	
		});
	},

	updateOne: function(req, res, next){

		accessor.fetchApplication(req, res, function(application, data_scheme){

			sanitize(req.body, allowed_params);

			var validation_schema = {
			  	"type" : "object",
			  	"properties" : {
			  		"name" : {"type" : "string"},
			  		"data_scheme" : {"data_scheme" : data_scheme},
			  		"url" : {"type" : "string"},
			  		"timezone" : {"timezone" : require("../data/timezone")}
			  	}
			 };

			var validator = ajv.compile(validation_schema);

			if(validator(req.body)){

			    Application.findOneAndUpdate({_id: req.params.app_id}, req.body, {new: true}, function(error, updated_application){
			       
			      	if(error){
						return new ResponseBuilder(res)
					     	.status(400)
					      	.meta(false)
					      	.errors(errors[400])
					      	.build()		      		
			      	}

			    	return new ResponseBuilder(res)
					   .status(200)
					   .meta(true)
					   .payload(updated_application)
					   .build()	 

			    });

			}else{
				return new ResponseBuilder(res)
					.status(422)
					.meta(false)
					.errors(validator.errors)
					.build()				
			}
		});
	},

	deleteOne: function(req, res, next){

		accessor.fetchApplication(req, res, function(application){
	      application.remove();

	      return new ResponseBuilder(res)
	      	.status(204)
	      	.meta(true)
	      	.payload({})
	      	.build()			
		});

	},
	
	createOne: function(req, res, next){

	  	accessor.getDataSchemes(function(data_scheme){

	  		sanitize(req.body, allowed_params);

			var validation_schema = {
			  	"type" : "object",
			  	"properties" : {
			  		"name" : {"type" : "string"},
			  		"data_scheme" : {"data_scheme" : Object.keys(data_scheme)},
			  		"url" : {"type" : "string"},
			  		"timezone" : {"timezone" : require("../data/timezone")}
			  	},
			  	"required" : ["name", "url", "timezone"]
			};

			var validator = ajv.compile(validation_schema);

		  	if(validator(req.body)){

			  	Application.create(req.body, function(err, application) {

			    	if (err){
				    	return new ResponseBuilder(res)
				     		.status(400)
				      		.meta(false)
				      		.errors(errors[400])
				      		.build()
			    	}

			    	return new ResponseBuilder(res)
			      		.status(201)
			      		.meta(true)
			      		.payload(application)
			      		.build()
			  	});

		  	}else{
				return new ResponseBuilder(res)
					.status(422)
					.meta(false)
					.errors(validator.errors)
					.build()
		  	}

	  	});

	},

	getCurrentEntry: function(req, res, next){
		accessor.fetchApplication(req, res, function(application){

			var offset = moment.tz.zone(application.timezone).parse(Date.UTC(Math.floor(Date.now()/1000))) * 60;

			var tree = application.getScheduleAsTree(offset);
			var schedules = tree.findIntervalsWithPoint(tree.getRoot(), Math.floor(Date.now()/1000) + offset);

			if(schedules.length === 0){
			    return new ResponseBuilder(res)
			     	.status(200)
			   		.meta(true)
		    		.payload({})
		      		.build()				
			}else{
			    return new ResponseBuilder(res)
			     	.status(200)
			   		.meta(true)
		    		.payload(schedules[0].payload)
		      		.build()				
			}
		});
	}
};