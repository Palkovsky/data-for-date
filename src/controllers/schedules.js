var ResponseBuilder = require('../lib/response');
var errors = require("../lib/errors/errors");
var error = require("../lib/error");
var sanitize = require("../lib/sanitizer");
var ajv = require("../lib/ajv/ajv");
var accessor = require('../lib/accessor');

var allowed_params = ["starts_at", "duration", "recurring", "entry_id"];

module.exports = {

	getAll : function(req, res, next){
		accessor.fetchApplication(req, res, function(application){
			return new ResponseBuilder(res)
			   	.status(200)
			  	.meta(true, true)
		      	.payload(application.schedules)
		      	.build()			
		});		
	},

	createOne : function(req, res, next){
		accessor.fetchApplication(req, res, function(application, data_scheme){

			sanitize(req.body, allowed_params);

			var validation_schema = {
			  	"type" : "object",
			  	"properties" : {
			  		"starts_at" : {"type" : "number", "minimum" : 0},
			  		"duration" : {"type" : "number", "minimum" : 3600},
			  		"recurring" : {"type" : "boolean"},
			  		"entry_id" : {"type" : "string"}
			  	},
			  	"required" : ["starts_at", "duration", "entry_id"]
			 };

			var validator = ajv.compile(validation_schema);

			if(validator(req.body)){

				//check if entry exists
				if(!accessor.doesEntryExists(application, req.body.entry_id)){
					return new ResponseBuilder(res)
				    	.status(404)
			    		.meta(false)
				   		.errors(error(404, 'Entry with given ID not found.'))
			     		.build()						
				}

				//check if given interval is free
				if(!application.isIntervalFree(req.body.starts_at, req.body.starts_at + req.body.duration)){
					return new ResponseBuilder(res)
			      		.status(400)
			      		.meta(false)
			      		.errors(error(400, 'There is somethig planed at this time.'))
			      		.build()	
				}

				var schedule = application.schedules.create(req.body);
				application.schedules.push(schedule);

				application.save(function(error){

					if(error){
						return new ResponseBuilder(res)
			      			.status(400)
			      			.meta(false)
			      			.errors(errors[400])
			      			.build()		
					}

					return new ResponseBuilder(res)
					   	.status(201)
					  	.meta(true)
				      	.payload(schedule)
				      	.build()

				});

			}else{
				return new ResponseBuilder(res)
					.status(422)
					.meta(false)
					.errors(validator.errors)
					.build()				
			}

		});
	},

	deleteOne : function(req, res, next){
		accessor.fetchSchedule(req, res, function(application, schedule){

			schedule.remove();

			application.save(function(err){
				if(err){
					return new ResponseBuilder(res)
			      		.status(400)
			      		.meta(false)
			      		.errors(errors[400])
			      		.build()				
				}

		     	return new ResponseBuilder(res)
			   		.status(204)
		     		.meta(true)
		      		.payload({})
		      		.build()							
			});

		});
	},

	updateOne : function(req, res, next){
		accessor.fetchSchedule(req, res, function(application, schedule){

			sanitize(req.body, allowed_params);

			var validation_schema = {
			  	"type" : "object",
			  	"properties" : {
			  		"starts_at" : {"type" : "number", "minimum" : 0},
			  		"duration" : {"type" : "number", "minimum" : 3600},
			  		"recurring" : {"type" : "boolean"},
			  		"entry_id" : {"type" : "string"}
			  	}
			 };

			var validator = ajv.compile(validation_schema);

			if(validator(req.body)){

				if('starts_at' in req.body){schedule.starts_at = req.body.starts_at;}
				if('duration' in req.body){schedule.duration = req.body.duration;}
				if('recurring' in req.body){schedule.recurring = req.body.recurring;}
				if('entry_id' in req.body){

					if(accessor.doesEntryExists(application, req.body.entry_id)){
						schedule.entry_id = req.body.entry_id;
					}else{
						return new ResponseBuilder(res)
				     		.status(404)
				      		.meta(false)
					   		.errors(error(404, 'Entry with given ID not found.'))
				     		.build()						
					}
				}

				//This code will check for interval intersections, but pass intervals of currently edited schedule
				var intersecting = false;
				var intersections = application.getIntersections(schedule.starts_at, schedule.starts_at + schedule.duration);
				for(var i = 0; i < intersections.length; i++){
					if(intersections[i].payload.id != schedule.id){
						intersecting = true;
						break;
					}
				}


				if(intersecting === true){
					return new ResponseBuilder(res)
			      		.status(400)
			      		.meta(false)
			      		.errors(error(400, 'There is somethig planed at this time.'))
			      		.build()	
				}

				application.save(function(err){


					if(err){
						return new ResponseBuilder(res)
					     	.status(400)
					      	.meta(false)
					      	.errors(errors[400])
					      	.build()		      		
			      	}

				    return new ResponseBuilder(res)
					   .status(200)
					   .meta(true)
					   .payload(schedule)
					   .build()	
				});

			}else{
				return new ResponseBuilder(res)
					.status(422)
					.meta(false)
					.errors(validator.errors)
					.build()				
			}			

		});
	}

};