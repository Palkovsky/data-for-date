var express = require('express');
var app = express();
require('./parser')(app);
require('./db')(app);
require('../../routes')(app);

var rescheduler = require('../jobs/reschedule');

//rescheduler.rescheduling(function(err){});

module.exports = app;