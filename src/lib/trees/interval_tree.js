var TreeBase = require('./treebase');

function Node(interval, payload) {

	//interval tree node separates from standard rb node with that
	//it contains interval attributes(low & high) and
	//max field(containing highest interval point of its children)
	
	//my implementation has also 'payload' field, where i store aditional data
	//required for application logic

	//do lil' validation of passed interval
	if(Array.isArray(interval) && interval.length === 2){

		if(interval[1] < interval[0])
			throw 'Incorect interval.';

		this.high = interval[1];
		this.low = interval[0];
		this.max = this.high;
	}else{
		this.high = null;
		this.low = null;
		this.max = null;
	}

	this.payload = payload;

	//done it this way to provide support to treebase
	//data field may be referred as key in some resources
	this.data = this.low;
	this.left = null;
	this.right = null;
	this.red = true;	
}

Node.prototype.get_child = function(dir) {
	return dir ? this.right : this.left;
};

Node.prototype.set_child = function(dir, val) {
	if(dir) {
		this.right = val;
	}else {
		this.left = val;
	}
};

function IntervalTree() {
	this._root = null;
	this._comparator = function(a, b) { return a - b; };
	this.size = 0;
}

IntervalTree.prototype = new TreeBase();


// returns true if inserted, false if duplicate
IntervalTree.prototype.insert = function(data, payload) {
	var ret = false;

	if(this._root === null) {
		// empty tree
		this._root = new Node(data, payload);
		ret = true;
		this.size++;
	}else {
		var head = new Node(undefined); // fake tree root

		var dir = 0;
		var last = 0;

		// setup
		var gp = null; // grandparent
		var ggp = head; // grand-grand-parent
		var p = null; // parent
		var node = this._root;
		ggp.right = this._root;

		// search down
		while(true) {
			if(node === null) {
				// insert new node at the bottom
				node = new Node(data, payload);
				p.set_child(dir, node);
				ret = true;
				this.size++;
			}else if(is_red(node.left) && is_red(node.right)) {
				// color flip
				node.red = true;
				node.left.red = false;
				node.right.red = false;
			}

			// fix red violation
			if(is_red(node) && is_red(p)) {
				var dir2 = ggp.right === gp;

				if(node === p.get_child(last)) {
					ggp.set_child(dir2, single_rotate(gp, !last));
				}else {
					ggp.set_child(dir2, double_rotate(gp, !last));
				}
			}

			var cmp = this._comparator(node.data, data[0]);

			// stop if found
			if(cmp === 0) {
				break;
			}

			last = dir;
			dir = cmp < 0;

			// update helpers
			if(gp !== null) {
				ggp = gp;
			}
			gp = p;
			p = node;
			node = node.get_child(dir);
		}

		// update root
		this._root = head.right;
	}

	// make root black
	this._root.red = false;

	return ret;
};

// returns true if removed, false if not found
IntervalTree.prototype.remove = function(data) {
	if(this._root === null) {
		return false;
	}

	var head = new Node(); // fake tree root
	var node = head;
	node.right = this._root;
	var p = null; // parent
	var gp = null; // grand parent
	var found = null; // found item
	var dir = 1;

	while(node.get_child(dir) !== null) {
		var last = dir;

		// update helpers
		gp = p;
		p = node;
		node = node.get_child(dir);

		var cmp = this._comparator(data[0], node.data);

		dir = cmp > 0;

		// save found node
		if(cmp === 0) {
			found = node;
		}

		// push the red node down
		if(!is_red(node) && !is_red(node.get_child(dir))) {
			if(is_red(node.get_child(!dir))) {
				var sr = single_rotate(node, dir);
				p.set_child(last, sr);
				p = sr;
			}else if(!is_red(node.get_child(!dir))) {
				var sibling = p.get_child(!last);
				if(sibling !== null) {
					if(!is_red(sibling.get_child(!last)) && !is_red(sibling.get_child(last))) {
						// color flip
						p.red = false;
						sibling.red = true;
						node.red = true;
					}else {
						var dir2 = gp.right === p;

						if(is_red(sibling.get_child(last))) {
							gp.set_child(dir2, double_rotate(p, last));
						}else if(is_red(sibling.get_child(!last))) {
							gp.set_child(dir2, single_rotate(p, last));
						}

						// ensure correct coloring
						var gpc = gp.get_child(dir2);
						gpc.red = true;
						node.red = true;
						gpc.left.red = false;
						gpc.right.red = false;
					}
				}
			}
		}
	}

	// replace and remove if found
	if(found !== null) {
		found.data = node.data;
		p.set_child(p.right === node, node.get_child(node.left === null));
		this.size--;
	}

	// update root and make it black
	this._root = head.right;
	if(this._root !== null) {
		this._root.red = false;
	}

	return found !== null;
};

//finds node by data
IntervalTree.prototype.find = function(data) {
    var res = this._root;

    while(res !== null) {
        var c = this._comparator(data, res.data);
        if(c === 0) {
            return res;
        }
        else {
            res = res.get_child(c > 0);
        }
    }

    return null;
};

IntervalTree.prototype.getRoot = function(){
	return this._root;
};

//finds all intersecting intervals with point as arg
IntervalTree.prototype.findIntervalsWithPoint = function(x, point){
	return this.findIntervals(x, [point, point]);
};

//finds all intersecting intervals
IntervalTree.prototype.findIntervals = function(x, interval) {

	if(this._root !== null){
	    var low = interval[0];
	    var high = interval[1];

	    var results = [];

	    if(low <= x.high && x.low <= high){
	    	results.push(x);
	    }

	    if(x.left !== null && x.left.max >= low){
	    	results = results.concat(this.findIntervals(x.left, interval));
	    }

	    if(x.right !== null && x.right.max >= low){
	    	results = results.concat(this.findIntervals(x.right, interval));
	    }

	    return results;
	}
	return [];
};

function is_red(node) {
	return node !== null && node.red;
}

function calculateMax(node){
	var max = node.high;
	if(node.right !== null && node.right.max > max)
		max = node.right.max;
	if(node.left !== null && node.left.max > max)
		max = node.left.max;
	return max;
}

function single_rotate(root, dir) {
	var save = root.get_child(!dir); //left or right child

	root.set_child(!dir, save.get_child(dir));
	save.set_child(dir, root);

	root.red = true;
	save.red = false;

	//recalculate max
	root.max = calculateMax(root);
	save.max = calculateMax(save);

	return save;
}

function double_rotate(root, dir) {
	root.set_child(!dir, single_rotate(root.get_child(!dir), !dir));
	return single_rotate(root, dir);
}

module.exports = IntervalTree;