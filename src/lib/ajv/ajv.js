var ajv = new require('ajv')();

ajv.addKeyword("data_scheme", {

  errors: true,
  validate: function data_scheme(value, dataObject) {
	
	data_scheme.errors = [];

  	if(value.indexOf(dataObject) === - 1){

	    data_scheme.errors.push({
	      message: "data_scheme attribute should be one of: " + value.toString(),
	      params: {
	      	property: "data_scheme",
	        values: value
	      }
	    });

	    return false;
	}
	return true;
  }
});

ajv.addKeyword("timezone", {

  errors: true,
  validate: function timezone(value, dataObject) {
	
	timezone.errors = [];

  	if(value.indexOf(dataObject) === - 1){

	    timezone.errors.push({
	      message: "timezone attribute should be one of: " + value.toString(),
	      params: {
	      	property: "timezone",
	        values: value
	      }
	    });

	    return false;
	}
	return true;
  }
});

var getType = function(obj){
	if(Array.isArray(obj))
		return "array";
	return typeof(obj);
}

var validate_object = function(input, scheme, errors, max_schema){

	var inputKeys = [];
	var schemeKeys = Object.keys(scheme);

	for(var key in input){
		if(input.hasOwnProperty(key)){

			if(scheme.hasOwnProperty(key)){

				inputKeys.push(key);
				var value = input[key];

				if(getType(value) === 'object'){
					validate_object(value, scheme[key], errors, max_schema);
				}else if(getType(value) === 'array' && Array.isArray(scheme[key]) && scheme[key].length === 1){

					for(var i = 0; i < value.length; i++){
						var item = value[i];
						validate_object(item, scheme[key][0], errors, max_schema);
					}

				}else{

					if(getType(value) !== scheme[key]){
						errors.push({
							message: "invalid type for '" + key + "'. should be: '" + scheme[key] + "'",
							params: {
								schema: max_schema,
								property: key,
								required_type: scheme[key],
								type: getType(value)
							}
						});
					}
				}
			}else{
				errors.push({
					message: "no '" + key + "' defined in schema",
					params: {
						schema: max_schema,
						property: key
					}
				});
			}
		}
	}

	//calculate missing keys
	var missingKeys = schemeKeys.filter(function(val){
		return inputKeys.indexOf(val) == -1;
	});

	for(var i = 0; i < missingKeys.length; i++){
		errors.push({
			message: "missing '" + missingKeys[i] + "' in your data",
			params: {
						schema: max_schema,
						missing: missingKeys[i]
				}
		});		
	}

};

ajv.addKeyword("scheme", {

	errors: true,
	validate: function scheme(value, dataObject){

		scheme.errors = [];
		if(value !== null){

			//dataObject is user input
			//value is one you've setup in validation scheme
			validate_object(dataObject, value, scheme.errors, value);
			return (scheme.errors.length === 0);
		}

		return true;	
	}

});


module.exports = ajv;