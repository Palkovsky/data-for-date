var mongoose = require('mongoose');

module.exports = function(app) { 
	mongoose.connect('mongodb://localhost/DATA_PER_DATE', {
		mongoose: { safe: true }
	},
	function(err) {
		if (err) {
		  return console.log('Mongoose - connection error:', err);
		}
	});
	return mongoose; 
};