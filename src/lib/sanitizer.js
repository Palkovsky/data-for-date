module.exports = function(body, allowed_params){
	for(var key in body){
		if(body.hasOwnProperty(key) && allowed_params.indexOf(key) === -1){
			delete body[key];
		}
	}
};