var error = require("../error");

/*
	Class used for standarized errors messages for common
	errors like 404 or 400.
*/


module.exports = {
	400 : error(400, "Unable to process request."),
	404 : error(404, "Requested entity wasn't found.")
};