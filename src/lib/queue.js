var kue = require('kue');

var queue = kue.createQueue({});

queue.watchStuckJobs(6000);

queue.on('ready', () => {  
  // If you need to 
  console.info('Queue is ready!');
});

queue.on('error', (err) => {  
  // handle connection errors here
  console.error('There was an error in the main queue!');
  console.error(err);
  console.error(err.stack);
});


module.exports = queue;