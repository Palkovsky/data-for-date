//Response builder for API endpoints

var ResponseBuilder = function(res){
	this.res = res;
	this.metadata = {};
	this.data_payload = null;
	this.pagination = null;
	this.err = [];
}

ResponseBuilder.prototype.status = function(status){
	this.metadata["code"] = status; 
	this.res.status(status);
	return this;
};

ResponseBuilder.prototype.meta = function(success, collection){
	this.metadata["success"] = success;
	if(typeof(collection) === 'boolean'){
		this.metadata["collection"] = collection;
	}else{
		this.metadata["collection"] =  false;
	}
	return this;
};

ResponseBuilder.prototype.pagination = function(current_page, next_page, total_pages, per_page){
	if(this.pagination === null){
		this.pagination = {};
	}
	this.pagination["current_page"] = current_page;
	this.pagination["next_page"] = next_page;
	this.pagination["total_pages"] = total_pages;
	this.pagination["per_page"] = per_page;
	return this;
};


ResponseBuilder.prototype.errors = function(errors){
	if(!(errors instanceof Array)){
		errors = [errors];
	}
	this.err = errors;
	return this;
};

ResponseBuilder.prototype.payload = function(payload){
	this.data_payload = payload;
	return this;
};

ResponseBuilder.prototype.build = function(){
	var response = {};

	if(this.data_payload !== null){
		response["data"] = this.data_payload;

		if(this.pagination !== null){
			response["pagination"] = this.pagination;
			this.metadata["paginated"] = true;
		}else{
			this.metadata["paginated"] = false;
		}
	}else{
		response["errors"] = this.err;
	}

	response["metadata"] = this.metadata;

	return this.res.json(response);
}

module.exports = ResponseBuilder;