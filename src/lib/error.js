//Class for creating error messages in API

var error = function(code, message, options){
	var e = {};

	e["code"] = code;
	e["message"] = message;

	if(typeof(options) === 'object'){
		for(var key in options){
			if(options.hasOwnProperty(key)){
				e[key] = options[key];
			}
		}
	}

	return e;
};

module.exports = error;