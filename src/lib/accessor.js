var mongoose = require('mongoose');
var DataScheme = require('../models/data_schemes');
var ResponseBuilder = require('../lib/response');
var errors = require("../lib/errors/errors");


module.exports = {

	getDataSchemes: function(callback){

		var ds = {'custom' : null};
		DataScheme.find({}).exec(function(err, data_schemes) {
			if(data_schemes instanceof Array){
				for(var i = 0; i < data_schemes.length; i++){
					var dsJSON = data_schemes[i].toJSON();
					var name = dsJSON['name'];
					var fields = dsJSON['fields'];
					ds[name] = fields;
				}
			}
			callback(ds);
		});	
	},

	fetchApplications : function(req, res, callback){
		mongoose.model('Application').find({}).exec(function(err, applications) {

	      if (err){
	      	return new ResponseBuilder(res)
		      		.status(400)
		      		.meta(false)
		      		.errors(errors[400])
		      		.build()
	      }

	      callback(applications);
		});
	},

	fetchApplication : function(req, res, callback){
		this.getDataSchemes(function(data_schemes){

			mongoose.model('Application').findOne({_id: req.params.app_id}).exec(function(err, application){

				if(err){
					return new ResponseBuilder(res)
				   		.status(400)
				   		.meta(false)
			      		.errors(errors[400])
			      		.build()				
				}

				if(!application){
					return new ResponseBuilder(res)
			    		.status(404)
			      		.meta(false)
				   		.errors(errors[404])
			     		.build()	
				}

				callback(application, data_schemes[application.data_scheme]);

			});
		});		
	},

	fetchEntry : function(req, res, callback){
		this.fetchApplication(req, res, function(application, data_scheme){

			var entry = application.entries.filter(function(entry){
				return entry._id == req.params.entry_id;
			}).pop();


			if(!entry){
				return new ResponseBuilder(res)
		     		.status(404)
		      		.meta(false)
			   		.errors(errors[404])
		     		.build()				
			}

			callback(application, entry, data_scheme);
		});		
	},

	fetchSchedule : function(req, res, callback){
		this.fetchApplication(req, res, function(application, data_scheme){

				var schedule = application.schedules.filter(function(schedule){
					return schedule.id == req.params.schedule_id;
				}).pop();


				if(!schedule){
					return new ResponseBuilder(res)
			      		.status(404)
			      		.meta(false)
			      		.errors(errors[404])
			      		.build()				
				}

				callback(application, schedule, data_scheme);
		});	
	},

	doesEntryExists : function(application, entry_id, callback){
		var entry = application.entries.filter(function(entry){
			return entry._id == entry_id;
		}).pop();
		return Boolean(entry);
	}
};