var moment = require('moment-timezone');
var should = require('should');
var assert = require('assert');
var factory = require('../factories');

describe('Schedule model', function(){

    var self = this;
    self.application = null;

    beforeEach(function(done){
    	factory.create('application', function(err, application) {
		  self.application = application;
		  done();
		});
    });


	it('should automatically calculate ends_at value', function(done){
		var s = self.application.schedules[0];
		s.ends_at.should.be.equal(s['starts_at'] + s['duration']);
		done();
	});

	it('should have nicely formated json response', function(done){
		var s = self.application.schedules[0];
		var json = s.toJSON();

		var starts_offset = moment.tz.zone(self.application.timezone).parse(Date.UTC(s.starts_at)) * 60;
		var ends_offset = moment.tz.zone(self.application.timezone).parse(Date.UTC(s.ends_at)) * 60;

		json.should.have.property('starts_at');
		json.starts_at.should.have.property('utc', s.starts_at);
		json.starts_at.should.have.property('converted', s.starts_at + starts_offset);
		json.starts_at.should.have.property('formated');
		json.starts_at.formated.should.have.property('utc', moment.utc(s.starts_at * 1000).format('DD-MM-YYYY HH:mm'));
		json.starts_at.formated.should.have.property('converted', moment.utc((s.starts_at + starts_offset) * 1000).format('DD-MM-YYYY HH:mm'));

		json.should.have.property('ends_at');
		json.ends_at.should.have.property('utc', s.ends_at);
		json.ends_at.should.have.property('converted', s.ends_at + ends_offset);
		json.ends_at.should.have.property('formated');
		json.ends_at.formated.should.have.property('utc', moment.utc(s.ends_at * 1000).format('DD-MM-YYYY HH:mm'));
		json.ends_at.formated.should.have.property('converted', moment.utc((s.ends_at + ends_offset) * 1000).format('DD-MM-YYYY HH:mm'));

		done();
	});

	it('should rebalance schedules', function(done){
		self.application.balance(function(){

			self.application.schedules.should.have.length(2);

			var scheduleA = self.application.schedules[0];
			var scheduleB = self.application.schedules[1]; //this is the balanced one

			scheduleB.starts_at.should.be.eql(scheduleA.ends_at);
			scheduleB.ends_at.should.be.eql(scheduleA.ends_at + scheduleB.duration);

			done();
		});
	});
});