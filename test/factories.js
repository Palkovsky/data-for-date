var FactoryGirl = require('factory-girl');
var factory = new FactoryGirl.Factory();
var MongooseAdapter = require('factory-girl-mongoose').MongooseAdapter;
factory.setAdapter(MongooseAdapter);

//Application factories definitions
var Application = require('../src/models/applications');
factory.define('application', Application, {
	name: "My App",
	url: "https://www.google.com",
	timezone: "America/Maceio",
	entries: [
		{
			"payload" : {"name" : "Jon", "surname" : "Nowak"}
		},
		{
			"payload" : {"name" : "Adom", "surname" : "Nowak"}
		},
		{
			"payload" : {"name" : "Szymon", "surname" : "Nowak"}
		}
	],
	schedules: [
		{
			'starts_at' : 1170659591, 'duration' : 3600, 'recurring' : true
		},{
			'starts_at' : Math.floor(Date.now()/1000), 'duration' : 3600, 'recurring' : true
		}
	]
});

factory.define('data_scheme_application', Application, {
	name: "My App",
	url: "https://www.google.com",
	timezone: "Africa/Asmera",
	data_scheme: "video",
	entries: []
});

module.exports = factory;