var should = require('should');
var moment = require('moment-timezone');
var request = require('supertest');
var assert = require('assert');
var factory = require('../factories');
var app = require('../../src/lib/app');

describe('Schedules', function() {

	describe('GET schedules', function(){

    	var self = this;
    	self.application = null;

    	before(function(done){
    		factory.create('application', function(err, application) {
			  self.application = application;
			  done();
			});
    	});	


    	it('should fetch schedules', function(done){
		    request(app).get('/apps/' + self.application["id"] + "/schedule").expect(200).end(function(err, res){
			   	res.body.should.have.property('metadata');
			   	res.body.metadata.should.have.property('success', true);
		    	res.body.data.should.have.length(2); //coz we defined 2 schedules in our factory
		    	res.body.data[0].should.have.property('id', self.application.schedules[0]['id']);
		    	res.body.data[1].should.have.property('id', self.application.schedules[1]['id']);
		    	done();
			});
		});
	});

	describe('DELETE schedule', function(){

    	var self = this;
    	self.application = null;
    	self.schedule = null;
    	self.total_schedules = null;

    	before(function(done){
    		factory.create('application', function(err, application) {
			  self.application = application;
			  self.total_schedules = self.application.schedules.length;
			  self.schedule = application.schedules[0];
			  done();
			});
    	});

    	it('should delete entry', function(done){
		    request(app).delete('/apps/' + self.application["id"] + "/schedule/" + self.schedule["id"]).expect(204, done);
    	});

    	it('should have been deleted', function(done){
		    request(app).get('/apps/' + self.application["id"] + "/schedule").expect(200).end(function(err, res){
			   	res.body.should.have.property('metadata');
			   	res.body.metadata.should.have.property('success', true);
			   	res.body.should.have.property('data');
		    	res.body.data.should.have.length(self.total_schedules - 1);
		    	done();
			});
    	});
	});

	describe('POST schedule', function(){

    	var self = this;
    	self.application = null;
    	self.entry = null;
    	self.new_schedule_id = null;

    	before(function(done){
    		factory.create('application', function(err, application) {
			  self.application = application;
			  self.entry = application.entries[0];
			  done();
			});
    	});

		it('should create new schedule', function(done){

			var schedule = {
				"starts_at" : Math.floor(Date.now()/1000) + 100000, "duration" : 86400, "recurring" : true, "entry_id" : self.entry['id']
			};

	      	request(app).post('/apps/' + self.application["id"] + "/schedule").send(schedule).expect(201).end(function(err, res){
		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', true);
		    	res.body.should.have.property('data');

				var starts_offset = moment.tz.zone(self.application.timezone).parse(Date.UTC(schedule.starts_at)) * 60;

				res.body.data.should.have.property('starts_at');	
		    	res.body.data.starts_at.should.have.property('utc', schedule.starts_at);
		    	res.body.data.starts_at.should.have.property('converted', schedule.starts_at + starts_offset);

		    	var ends_offset = moment.tz.zone(self.application.timezone).parse(Date.UTC(schedule.ends_at + schedule.duration)) * 60;

		    	res.body.data.should.have.property('ends_at');
		    	res.body.data.ends_at.should.have.property('utc', schedule.starts_at + schedule.duration);
		    	res.body.data.ends_at.should.have.property('converted', schedule.starts_at + schedule.duration + ends_offset);

		    	res.body.data.should.have.property('duration', schedule.duration);
		    	res.body.data.should.have.property('recurring', schedule.recurring);

		    	res.body.data.should.have.property("entry");
		    	res.body.data.entry.should.have.property('id', self.entry['id']);

		    	res.body.data.should.have.property('id');

		    	self.new_schedule_id = res.body.data['id'];

		    	done();
			});

		});

		it('should have been created', function(done){
	      	request(app).get('/apps/' + self.application["id"] + '/schedule').expect(200).end(function(err, res){
		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', true);
		    	res.body.should.have.property('data');
		    	res.body.data.should.have.length(3);
		    	res.body.data[0].should.have.property('id', self.application.schedules[0]['id']);
		    	res.body.data[1].should.have.property('id', self.application.schedules[1]['id']);
		    	res.body.data[2].should.have.property('id', self.new_schedule_id);		    	
		    	done();
			});
		});

		it('should refuse to create new schedule due to missing duration', function(done){
			var schedule = {"starts_at" : Math.floor(Date.now()/1000) + 100000, "recurring" : true, "entry_id" : self.entry['id']};

	      	request(app).post('/apps/' + self.application["_id"] + "/schedule").send(schedule).expect(422).end(function(err, res){
		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', false);
		    	done();
			});
		});

		it('should refuse to create new schedule due to negative duration', function(done){
			var schedule = {
				"starts_at" : Math.floor(Date.now()/1000) + 100000, "duration" : -86400, "recurring" : true, "entry_id" : self.entry['id']
			};

	      	request(app).post('/apps/' + self.application["id"] + "/schedule").send(schedule).expect(422).end(function(err, res){
		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', false);
		    	done();
			});
		});

		it('should refuse to create new schedule due to incorect entry id', function(done){
			var schedule = {"starts_at" : Math.floor(Date.now()/1000) + 100000, "duration" : 3600, "recurring" : true, "entry_id" : 'S.E.X'};

	      	request(app).post('/apps/' + self.application["id"] + "/schedule").send(schedule).expect(404).end(function(err, res){
		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', false);
		    	done();
			});
		});

		it('should refuse to create new schedule due to intersecting intervals', function(done){
			var schedule = {"starts_at" : Math.floor(Date.now()/1000), "duration" : 86400, "recurring" : true, "entry_id" : self.entry['id']};

	      	request(app).post('/apps/' + self.application["id"] + "/schedule").send(schedule).expect(400).end(function(err, res){
		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', false);
		    	done();
			});
		});

	});

	describe('PATCH schedule', function(){

    	var self = this;
    	self.application = null;
    	self.schedule = null;

    	beforeEach(function(done){
    		factory.create('application', function(err, application) {
			  self.application = application;
			  self.schedule = application.schedules[0];
			  done();
			});
    	});		

    	it('should edit schedule', function(done) {
	      	var schedule = {
	        	'starts_at' : 1470659591,
	        	'duration' : 86400
	    	};

	    	request(app).patch('/apps/' + self.application["id"] + '/schedule/' + self.schedule["id"]).send(schedule).expect(200, function(err, res){
		    	

		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', true);
		    	res.body.should.have.property('data');

				var starts_offset = moment.tz.zone(self.application.timezone).parse(Date.UTC(schedule.starts_at)) * 60;

				res.body.data.should.have.property('starts_at');	
		    	res.body.data.starts_at.should.have.property('utc', schedule.starts_at);
		    	res.body.data.starts_at.should.have.property('converted', schedule.starts_at + starts_offset);

		    	var ends_offset = moment.tz.zone(self.application.timezone).parse(Date.UTC(schedule.ends_at + self.schedule.duration)) * 60;

		    	res.body.data.should.have.property('ends_at');
		    	res.body.data.ends_at.should.have.property('utc', schedule.starts_at + schedule.duration);
		    	res.body.data.ends_at.should.have.property('converted', schedule.starts_at + schedule.duration + ends_offset);

		    	res.body.data.should.have.property('duration', schedule.duration);
		    	res.body.data.should.have.property('recurring', self.schedule.recurring);

		    	res.body.data.should.have.property('id', self.schedule['id']);

	    		done();
	    	});
	    });

		it('should refuse to update schedule due to incorect entry id', function(done){
			var schedule = {"starts_at" : Math.floor(Date.now()/1000) + 100000, "duration" : 3600, "recurring" : true, "entry_id" : 'S.E.X'};

	      	request(app).patch('/apps/' + self.application["id"] + "/schedule/" + self.schedule['id']).send(schedule).expect(404).end(function(err, res){
		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', false);
		    	done();
			});
		});

		it('should refuse to update schedule due to negative duration', function(done){
			var schedule = {"duration" : -3600};

	      	request(app).patch('/apps/' + self.application["id"] + "/schedule/" + self.schedule['id']).send(schedule).expect(422).end(function(err, res){
		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', false);
		    	done();
			});
		});

		it('should refuse to update schedule due to intersecting intervals', function(done){
			var schedule = {"starts_at" : Math.floor(Date.now()/1000) + 3599};

	      	request(app).patch('/apps/' + self.application["id"] + "/schedule/" + self.schedule['id']).send(schedule).expect(400).end(function(err, res){
		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', false);
		    	done();
			});
		});

	});
});