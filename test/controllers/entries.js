var should = require('should');
var request = require('supertest');
var assert = require('assert');
var factory = require('../factories');
var app = require('../../src/lib/app');

describe('Entries', function() {

	describe('validate entry schema', function(){

	    	var self = this;
	    	self.application = null;

	    	//Scheme 'video', which looks like this
	    	/*
	    		{
					"name" : "video"
					"fields" : {"url" : "string"}
				}
	    	*/

	    	before(function(done){
	    		factory.create('data_scheme_application', function(err, application) {
				  self.application = application;
				  done();
				});
	    	});

	    	it('should return no field in schema error', function(done){
	    		var data = {"payload" : {"uri" : "http://www.google.com", "url" : "http://www.wp.pl"}}

		      	request(app).post('/apps/' + self.application["_id"] + "/entries").send(data).expect(422).end(function(err, res){
			    	res.body.should.have.property('metadata');
			    	res.body.metadata.should.have.property('success', false);
			    	res.body.should.have.property('errors');
			    	res.body.errors.should.have.length(1);
			    	res.body.errors[0].should.have.property('message', "no 'uri' defined in schema")
			    	done();
				});    		
	    	});

	    	it('should return invalid type in schema error', function(done){
	    		var data = {"payload" : {"url" : 123}};

		      	request(app).post('/apps/' + self.application["_id"] + "/entries").send(data).expect(422).end(function(err, res){
			    	res.body.should.have.property('metadata');
			    	res.body.metadata.should.have.property('success', false);
			    	res.body.should.have.property('errors');
			    	res.body.errors.should.have.length(1);
			    	res.body.errors[0].should.have.property('message', "invalid type for 'url'. should be: 'string'")
			    	done();
	    	});

		    it('should return missing key in schema error', function(done){
		    	var data = {"payload" : {"uri" : "http://www.google.com"}};

		      	request(app).post('/apps/' + self.application["_id"] + "/entries").send(data).expect(422).end(function(err, res){
			    	res.body.should.have.property('metadata');
			    	res.body.metadata.should.have.property('success', false);
			    	res.body.should.have.property('errors');
			    	res.body.errors.should.have.length(1);
			    	res.body.errors[0].should.have.property('message', "missing 'url' in your data")
			    	done();		    	
		    	});
			});
		});
    });


	describe('POST entry', function(){

    	var self = this;
    	self.application = null;

    	before(function(done){
    		factory.create('application', function(err, application) {
			  self.application = application;
			  done();
			});
    	});

		it('should create new entry', function(done){

			var entry = {
				"payload" : {"url" : "http://www.google.com"}
			};

	      	request(app).post('/apps/' + self.application["_id"] + "/entries").send(entry).expect(201).end(function(err, res){
		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', true);
		    	res.body.data.payload.should.have.property("url", "http://www.google.com");
		    	done();
			});

		});

		it('should have been created', function(done){
	      	request(app).get('/apps/' + self.application["_id"]).expect(200).end(function(err, res){
		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', true);
		    	res.body.data.entries.should.have.length(4);
		    	done();
			});
		});

		it('should refuse to create new entry', function(done){
			var bad_entry = {"payload" : 23}

	      	request(app).post('/apps/' + self.application["_id"] + "/entries").send(bad_entry).expect(422).end(function(err, res){
		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', false);
		    	done();
			});

		});

	});


	describe('GET entries', function(){

    	var self = this;
    	self.application = null;

    	before(function(done){
    		factory.create('application', function(err, application) {
			  self.application = application;
			  done();
			});
    	});		

		it('should fetch entries', function(done){
		    request(app).get('/apps/' + self.application["_id"] + "/entries").expect(200).end(function(err, res){
			   	res.body.should.have.property('metadata');
			   	res.body.metadata.should.have.property('success', true);
		    	res.body.data.should.have.length(3);
		    	done();
			});
		});
	});

	describe('GET entry', function(){

    	var self = this;
    	self.application = null;
    	self.entry = null;

    	before(function(done){
    		factory.create('application', function(err, application) {
			  self.application = application;
			  self.entry = self.application.entries[0];
			  done();
			});
    	});

		it('should fetch entry', function(done){
		    request(app).get('/apps/' + self.application["_id"] + "/entries/" + self.entry['_id']).expect(200).end(function(err, res){
			   	res.body.should.have.property('metadata');
			   	res.body.metadata.should.have.property('success', true);
		    	done();
			});
		});

	});

	describe('DELETE entry', function(){

    	var self = this;
    	self.application = null;
    	self.entry = null;

    	before(function(done){
    		factory.create('application', function(err, application) {
			  self.application = application;
			  self.entry = application.entries[0];
			  done();
			});
    	});

    	it('should delete entry', function(done){
		    request(app).delete('/apps/' + self.application["_id"] + "/entries/" + self.entry["_id"]).expect(204, done);
    	});

    	it('should have been deleted', function(done){
		    request(app).get('/apps/' + self.application["_id"] + "/entries").expect(200).end(function(err, res){
			   	res.body.should.have.property('metadata');
			   	res.body.metadata.should.have.property('success', true);
		    	res.body.data.should.have.length(2);
		    	done();
			});
    	});
	});

	describe('PATCH entry', function(){

    	var self = this;
    	self.application = null;
    	self.entry = null;

    	beforeEach(function(done){
    		factory.create('application', function(err, application) {
			  self.application = application;
			  self.entry = application.entries[0];
			  done();
			});
    	});

    	it('should edit entry', function(done) { 
	      	var entry = {
	        	'payload': {'url' : 'homo-niewiadomo'}
	    	};

	    	request(app).patch('/apps/' + self.application["_id"] + '/entries/' + self.entry["_id"]).send(entry).expect(200, function(err, res){
	    		res.body.should.have.property('metadata');
	    		res.body.metadata.should.have.property('success', true);
	    		res.body.should.have.property('data');
	    		res.body.data.should.have.property('payload', {'url' : 'homo-niewiadomo'});
	    		done();
	    	});
	    });

    	it('should fail to edit entry and return unprocessable entity', function(done){
    		var entry = {
    			'payload' : 222
    		}

	    	request(app).patch('/apps/' + self.application["_id"] + '/entries/' + self.entry["_id"]).send(entry).expect(422, function(err, res){
	    		res.body.should.have.property('metadata');
	    		res.body.metadata.should.have.property('success', false);
	    		done();
	    	});
    	});

    	it('should fail to edit entry and return not found error', function(done){

	      	var entry = {
	        	'payload': {'url' : 'homo-niewiadomo'}
	    	};

	    	request(app).patch('/apps/' + (self.application["_id"] + 100) + '/entries/' + self.entry["_id"]).send(entry).expect(404, function(err, res){
	    		res.body.should.have.property('metadata');
	    		res.body.metadata.should.have.property('success', false);
	    		done();
	    	});    		

    	});

	});

});