var should = require('should');
var request = require('supertest');
var assert = require('assert');
var factory = require('../factories');
var app = require('../../src/lib/app');

describe('Applications', function() {

  	describe('POST application', function(){

	    it('should create an application', function(done){
	       
	       var application = {
	        name: 'My App',
	        url: 'http://www.google.com',
	        timezone: 'UTC'
	       };

	       request(app).post('/apps').send(application).expect(201).end(function(err, res){
	       		res.body.should.have.property('metadata');
	    		res.body.metadata.should.have.property('success', true);
	    		res.body.should.have.property('data');
	    		res.body.data.should.have.property('name', 'My App');
	    		done();
	    	});
	    });

	    it('should refuse to create an application', function(done){
	    	var bad_application = {
	    		name : 'My App',
	    		url: 'www.google.com',
	    		timezone: 'UTC-05:00'
	    	}
		    
		    request(app).post('/apps').send(bad_application).expect(422).end(function(err, res){
		    	res.body.should.have.property('errors');
		    	res.body.metadata.should.have.property('success', false);
		    	done();
		    });

	    });

	});


    describe('GET application', function() {

    	var self = this;
    	self.application = null;

    	before(function(done){
    		factory.create('application', function(err, application) {
			  self.application = application;
			  done();
			});
    	});

	    it('should retrieve application from db', function(done){ 
	      
	      request(app).get('/apps/' + self.application["_id"]).expect(200).end(function(err, res){
		    	res.body.should.have.property('metadata');
		    	res.body.metadata.should.have.property('success', true);
		    	res.body.data.should.have.property('entries');
		    	res.body.data.entries.should.have.length(3);
		    	done();
		    });

	    });
	});
	   
	describe('DELETE application', function() {

    	var self = this;
    	self.application = null;

    	before(function(done){
    		factory.create('application', function(err, application) {
			  self.application = application;
			  done();
			});
    	});

	    it('should remove an application', function(done) { 
	      request(app).delete('/apps/' + self.application["_id"]).expect(204, done);
	    });

	    it('application should have been deleted', function(done){
	    	request(app).get('/apps/' + self.application["_id"]).expect(404, done);
	    });

	});

	describe('PUT application', function() {

    	var self = this;
    	self.application = null;

    	before(function(done){
    		factory.create('application', function(err, application) {
			  self.application = application;
			  done();
			});
    	});

	    it('should edit an application', function(done) { 
	      	var application = {
	        	'name': 'Another App Name' 
	    	};

	    	request(app).patch('/apps/' + self.application["_id"]).send(application).expect(200, function(err, res){
	    		res.body.should.have.property('metadata');
	    		res.body.metadata.should.have.property('success', true);
	    		res.body.data.should.have.property('name', 'Another App Name');
	    		done();
	    	});
	    });

	    it('should have been edited', function(done){

	    	request(app).get('/apps/' + self.application["_id"]).expect(200).end(function(err, res){
	    		res.body.should.have.property('metadata');
	    		res.body.metadata.should.have.property('success', true);
	    		res.body.data.should.have.property('name', 'Another App Name');
	    		done();
	    	});

	    });
	});


	describe('GET current entry', function(){
    	var self = this;
    	self.application = null;
    	self.entry = null;

    	before(function(done){
    		factory.create('application', function(err, application) {
			  self.application = application;
			  self.entry = self.application.entries[0];
			  self.application.schedules[self.application.schedules.length - 1].entry_id = self.entry['id'];
			  self.application.save(function(err){
			  	done();
			  });
			});
    	});

    	it('should return one current entry', function(done){

	    	request(app).get('/apps/' + self.application["_id"] + '/current').expect(200).end(function(err, res){
	    		res.body.should.have.property('metadata');
	    		res.body.metadata.should.have.property('success', true);
	    		res.body.should.have.property('data');
	    		res.body.data.should.have.property('id', self.application.schedules[self.application.schedules.length - 1]['id']);
	    		res.body.data.should.have.property('duration', 3600);
	    		res.body.data.should.have.property('entry');
	    		res.body.data.entry.should.have.property('id', self.entry['id']);
	    		done();
	    	});   

    	});
	});
});