var should = require('should');
var request = require('supertest');
var assert = require('assert');
var accessor = require("../src/lib/accessor");
var ajv = require("../src/lib/ajv/ajv");

describe('Validators', function(){

	describe('schema validator', function(){


		it('should return invalid type error', function(done){

			var data = {"url" : [2, 3]};
			var data_scheme = {"url" : "string"};
			var validation_scheme = {
			  	"type" : "object",
			  	"scheme" : data_scheme
			};

			var validator = ajv.compile(validation_scheme);
			validator(data);

			validator.errors.should.have.length(1);
			validator.errors[0].should.have.property('params');
			validator.errors[0]['params'].should.have.property('required_type', 'string');
			validator.errors[0]['params'].should.have.property('type', 'array');

			done();
		});

		it('should return invalid field error', function(done){

			var data = {"uri" : "http://www.wp.pl", "url" : "http://www.interia.pl"};
			var data_scheme = {"url" : "string"};
			var validation_scheme = {
			  	"type" : "object",
			  	"scheme" : data_scheme
			};

			var validator = ajv.compile(validation_scheme);
			validator(data);

			validator.errors.should.have.length(1);
			validator.errors[0].should.have.property('params');
			validator.errors[0]['params'].should.have.property('property', 'uri');

			done();
		});


		it('should return missing field error', function(done){

			var data = {"url" : "http://www.wp.pl"};
			var data_scheme = {"url" : "string", "field" : "string"};
			var validation_scheme = {
			  	"type" : "object",
			  	"scheme" : data_scheme
			};

			var validator = ajv.compile(validation_scheme);
			validator(data);

			validator.errors.should.have.length(1);
			validator.errors[0].should.have.property('params');
			validator.errors[0]['params'].should.have.property('missing', 'field');

			done();
		});

	});

});