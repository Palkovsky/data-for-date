module.exports = function(grunt) {
	grunt.loadNpmTasks('grunt-mocha-test');
	
	grunt.initConfig({
		mochaTest: {
	   	 	test: {
	   	 		options: {
	   	 			reporter: 'spec'
	   	 		},
	      		src: ["test/**.js", "test/controllers/**.js", "test/models/**.js"]
	    	}
	  	}
  	});

  	grunt.registerTask('test', 'mochaTest');
};